package eu.rsulkowski.pvx

import android.database.sqlite.SQLiteDatabase

class LocationsRepository(private val database: SQLiteDatabase) {

    companion object {
        private const val HOLDER_ID_COL = "HolderId"
        private const val NAME_COL = "Name"
        private const val BARCODE_COL = "Barcode"
        private const val QUANTITY_OF_ITEMS_COL = "QuantityOfItems"
    }

    fun getAllLocations() : List<ProductLocation> {
        val productLocations = mutableListOf<ProductLocation>()

        val cursor = database.query("Holder", arrayOf(HOLDER_ID_COL, NAME_COL, BARCODE_COL, QUANTITY_OF_ITEMS_COL), null, null, null, null, null)
        cursor?.apply {
            moveToFirst()
            while(!cursor.isAfterLast) {
                val productLocation = ProductLocation(
                    holderId = cursor.getLong(0),
                    name = cursor.getString(1),
                    barcode = cursor.getString(2),
                    quantity = cursor.getInt(3)
                )
                productLocations.add(productLocation)
                cursor.moveToNext()
            }
            close()
        }

        return productLocations
    }

    fun getLocationDetails(locationId: Long) : List<Detail> {
        val details = mutableListOf<Detail>()

        val sqlQuery = "SELECT t1.Name FROM ItemType AS t1 WHERE t1.ItemTypeId = (SELECT t2.ItemTypeId FROM ItemTypeHolder AS t2 WHERE t2.HolderId == $locationId)"

        val cursor = database.rawQuery(sqlQuery, null)
        cursor?.apply {
            moveToFirst()
            while(!cursor.isAfterLast) {
                val detail = Detail(cursor.getString(0),0)
                details.add(detail)
                cursor.moveToNext()
            }
            close()
        }

        return details
    }

}

data class ProductLocation(val holderId: Long, val name: String, val barcode: String, val quantity: Int)