package eu.rsulkowski.pvx

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_locations.*
import kotlinx.android.synthetic.main.product_location_list_item.view.*

class LocationsFragment : Fragment() {

    companion object {
        fun create(): Fragment = LocationsFragment()
    }

    private var locationsRepository: LocationsRepository? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_locations, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.apply {
            locationsRepository = ViewModelProvider
                .AndroidViewModelFactory.getInstance(application)
                .create(MainViewModel::class.java)
                .locationsRepository

            locationsRepository?.apply {
                locationsListRv.adapter = LocationsListAdapter(getAllLocations())
                locationsListRv.layoutManager = LinearLayoutManager(context)
            }
        }
    }
}

class LocationsListAdapter(locations : List<ProductLocation> ) : RecyclerView.Adapter<ProductLocationViewHolder>() {

    private val locationsList: List<ProductLocation> = locations.toList()

    override fun onBindViewHolder(holder: ProductLocationViewHolder, position: Int) {
        holder.bind(locationsList[position])
    }

    override fun getItemCount(): Int = locationsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductLocationViewHolder =
            ProductLocationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_location_list_item, parent, false))

}

class ProductLocationViewHolder(view: View): RecyclerView.ViewHolder(view) {
    fun bind(productLocation: ProductLocation) {
        itemView.itemName.text = productLocation.name
        itemView.itemBarcode.text = productLocation.barcode
        itemView.itemQuantity.text = "(${productLocation.quantity})"
        itemView.setOnClickListener {
            DetailsActivity.startForLocations(itemView.context, productLocation.holderId, productLocation.name)
        }
    }
}