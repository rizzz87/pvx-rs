package eu.rsulkowski.pvx.entities

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("t") val tables: List<Table>
)

data class Table(
    @SerializedName("n") val name: String,
    @SerializedName("c") val columnNames: List<String>,
    @SerializedName("r") val rows: List<Row>
)

data class Row(
    @SerializedName("v") val values: List<Any?>
)