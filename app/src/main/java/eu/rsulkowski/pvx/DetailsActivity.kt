package eu.rsulkowski.pvx

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.detail_list_item.view.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        private const val DETAILS_SCREEN_NAME_KEY = "DETAILS_SCREEN_NAME_KEY"
        private const val LOCATION_ID_KEY = "LOCATION_ID"
        private const val ITEM_ID_KEY = "ITEM_ID"
        private const val DETAIL_NAME_KEY = "ITEM_NAME"

        fun startForLocations(context: Context, locationId: Long, name: String) {
            val startIntent = Intent(context, DetailsActivity::class.java)
            startIntent.putExtra(DETAILS_SCREEN_NAME_KEY, context.getString(R.string.label_location_details_title))
            startIntent.putExtra(DETAIL_NAME_KEY, name)
            startIntent.putExtra(LOCATION_ID_KEY, locationId)
            context.startActivity(startIntent)
        }

        fun startForItems(context: Context, itemId: Long, name: String) {
            val startIntent = Intent(context, DetailsActivity::class.java)
            startIntent.putExtra(DETAILS_SCREEN_NAME_KEY, context.getString(R.string.label_item_details_title))
            startIntent.putExtra(DETAIL_NAME_KEY, name)
            startIntent.putExtra(ITEM_ID_KEY, itemId)
            context.startActivity(startIntent)
        }
    }

    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setupViewModel()
        setupDetailsListView()
        intent.apply {
            title = this.getStringExtra(DETAILS_SCREEN_NAME_KEY)
            detailNameTv.text = this.getStringExtra(DETAIL_NAME_KEY)
            when (title) {
                baseContext.getString(R.string.label_location_details_title) -> proceedWithSelectedLocation(
                    this.getLongExtra(
                        LOCATION_ID_KEY,
                        -1
                    )
                )
                else -> proceedWithSelectedItem(this.getLongExtra(ITEM_ID_KEY, -1))
            }
        }
    }

    private lateinit var detailsListAdapter: DetailsListAdapter

    private fun setupDetailsListView() {
        detailsListAdapter = DetailsListAdapter()
        detailsRv.adapter = detailsListAdapter
        detailsRv.layoutManager = LinearLayoutManager(baseContext)
    }

    private fun proceedWithSelectedItem(itemId: Long) {
        val details = mainViewModel.itemsRepository?.getItemDetails(itemId) ?: listOf()
        detailsListAdapter.updateData(details)
        if (details.isEmpty()) {
            Toast.makeText(baseContext, getString(R.string.msg_no_locations_for_item), Toast.LENGTH_LONG).show()
        }
    }

    private fun proceedWithSelectedLocation(locationId: Long) {
        val details = mainViewModel.locationsRepository?.getLocationDetails(locationId) ?: listOf()
        detailsListAdapter.updateData(details)
        if (details.isEmpty()) {
            Toast.makeText(baseContext, getString(R.string.msg_empty_location), Toast.LENGTH_LONG).show()
        }
    }

    private fun setupViewModel() {
        mainViewModel =
                ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(MainViewModel::class.java)
    }
}


class DetailsListAdapter() : RecyclerView.Adapter<DetailViewHolder>() {

    private val details: MutableList<Detail> = mutableListOf()

    fun updateData(updatedDetailsList: List<Detail>) {
        details.clear()
        details.addAll(updatedDetailsList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.bind(details[position])
    }

    override fun getItemCount(): Int = details.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder =
        DetailViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.detail_list_item, parent, false))
}

class DetailViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(detail: Detail) {
        itemView.specificDetailNameTv.text = detail.name
        itemView.quantityTv.text = "${detail.quantity}"

    }

}

data class Detail(val name: String, val quantity: Int)