package eu.rsulkowski.pvx

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_items.*
import kotlinx.android.synthetic.main.product_location_list_item.view.*

class ItemsFragment : Fragment() {

    companion object {
        fun create() : Fragment = ItemsFragment()
    }

    private var itemsRepository: ItemsRepository? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_items, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.apply {
            itemsRepository = ViewModelProvider
                .AndroidViewModelFactory.getInstance(application)
                .create(MainViewModel::class.java)
                .itemsRepository

            itemsRepository?.apply {
                itemsListRv.adapter = ItemsListAdapter(getAllItems())
                itemsListRv.layoutManager = LinearLayoutManager(context)
            }
        }
    }
}

class ItemsListAdapter(locations : List<Item> ) : RecyclerView.Adapter<ItemViewHolder>() {

    private val itemsList: List<Item> = locations.toList()

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(itemsList[position])
    }

    override fun getItemCount(): Int = itemsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_location_list_item, parent, false))

}

class ItemViewHolder(view: View): RecyclerView.ViewHolder(view) {
    fun bind(item: Item) {
        itemView.itemName.text = item.name
        itemView.itemBarcode.text = item.barcode
        itemView.itemQuantity.text = "(${item.quantity})"
        itemView.setOnClickListener {
            DetailsActivity.startForItems(itemView.context, item.itemId, item.name)
        }
    }
}