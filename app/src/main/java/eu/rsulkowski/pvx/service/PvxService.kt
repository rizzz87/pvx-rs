package eu.rsulkowski.pvx.service

import eu.rsulkowski.pvx.entities.Response
import io.reactivex.Observable
import retrofit2.http.GET

interface PvxService {

    @GET("/test.json")
    fun obtainData() : Observable<Response>

}