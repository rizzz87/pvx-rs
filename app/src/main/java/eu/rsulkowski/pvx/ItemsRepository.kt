package eu.rsulkowski.pvx

import android.database.sqlite.SQLiteDatabase
import androidx.core.database.getIntOrNull

class ItemsRepository(private val database: SQLiteDatabase) {

    companion object {
        private const val ITEM_ID_COL = "ItemTypeId"
        private const val NAME_COL = "Name"
        private const val BARCODE_COL = "Barcode"
        private const val QUANTITY_COL = "Quantity"
    }

    fun getAllItems(): List<Item> {
        val items = mutableListOf<Item>()

        val sqlQuery =
            "SELECT t1.$ITEM_ID_COL, t1.$NAME_COL, t1.$BARCODE_COL, (SELECT SUM(t2.$QUANTITY_COL) FROM ItemTypeHolder AS t2 WHERE t2.$ITEM_ID_COL == t1.$ITEM_ID_COL) FROM ItemType AS t1"

        val cursor = database.rawQuery(sqlQuery, null)

        cursor?.apply {
            moveToFirst()
            while (!cursor.isAfterLast) {
                val item = Item(
                    itemId = cursor.getLong(0),
                    name = cursor.getString(1),
                    barcode = cursor.getString(2),
                    quantity = cursor.getIntOrNull(3) ?: 0
                )
                items.add(item)
                cursor.moveToNext()
            }
            close()
        }

        return items
    }

    fun getItemDetails(itemId: Long): List<Detail> {
        val details = mutableListOf<Detail>()

        val sqlQuery = "SELECT t1.Name FROM Holder AS t1 INNER JOIN ItemTypeHolder AS t2 ON t1.HolderId == t2.HolderId WHERE t2.ItemTypeId == $itemId"

        val cursor = database.rawQuery(sqlQuery, null)
        cursor?.apply {
            moveToFirst()
            while(!cursor.isAfterLast) {
                val detail = Detail(cursor.getString(0),0)
                details.add(detail)
                cursor.moveToNext()
            }
            close()
        }

        return details
    }
}


data class Item(val itemId: Long, val name: String, val barcode: String, var quantity: Int)