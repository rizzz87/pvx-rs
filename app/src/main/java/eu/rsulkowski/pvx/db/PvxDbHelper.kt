package eu.rsulkowski.pvx.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import eu.rsulkowski.pvx.entities.Response
import eu.rsulkowski.pvx.entities.Table

class PvxDbHelper(
    private val serviceResponse: Response? = null,
    context: Context,
    version: Int = 1
) : SQLiteOpenHelper(context, DB_NAME, null, version) {

    companion object {
        const val DB_NAME = "pvx.db"
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    override fun onCreate(db: SQLiteDatabase?) {
        serviceResponse?.apply {
            tables.forEach { t ->
                db?.apply {
                    createDatabase(db, t)
                    fillDatabaseWithData(t, db)
                }
            }
        }
    }

    private fun fillDatabaseWithData(t: Table, db: SQLiteDatabase) {
        t.rows.forEach {
            db.execSQL(
                "INSERT INTO ${t.name} (${t.columnNames.joinToString(",")}) VALUES (${it.values.joinToString(
                    " , ",
                    transform = { v -> "\"$v\"" })})"
            )
        }
    }

    private fun createDatabase(db: SQLiteDatabase, t: Table) {
        db.execSQL(
            "CREATE TABLE ${t.name} (${t.columnNames.joinToString(",")})"
        )
    }
}
