package eu.rsulkowski.pvx

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.SerialDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var mainViewModel: MainViewModel

    private val serialDisposable: SerialDisposable = SerialDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        serialDisposable.set(mainViewModel.completableDbCreation
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setupPager()
                setupTabLayout()
            })
    }

    override fun onStop() {
        super.onStop()
        if(!serialDisposable.isDisposed) {
            serialDisposable.dispose()
        }
    }

    private fun setupViewModel() {
        mainViewModel =
                ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(MainViewModel::class.java)
    }

    private fun setupPager() {
        val adapter = PagerAdapter(supportFragmentManager, tabLayout.tabCount)
        pager.adapter = adapter
        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }

    private fun setupTabLayout() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }


    inner class PagerAdapter(fragmentManager: FragmentManager, private val tabCount: Int) :
        FragmentStatePagerAdapter(fragmentManager) {

        override fun getCount(): Int = tabCount

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> LocationsFragment.create()
            1 -> ItemsFragment.create()
            else -> Fragment()
        }
    }

}

