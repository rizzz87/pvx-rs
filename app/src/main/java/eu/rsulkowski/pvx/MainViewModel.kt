package eu.rsulkowski.pvx

import android.app.Application
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import eu.rsulkowski.pvx.db.PvxDbHelper
import eu.rsulkowski.pvx.service.PvxService
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val disposables = CompositeDisposable()
    private var database: SQLiteDatabase? = null

    var locationsRepository: LocationsRepository? = null
    var itemsRepository: ItemsRepository? = null

    val completableDbCreation: Completable

    init {
        completableDbCreation = Completable.create { emitter ->
            if (!doesDatabaseExist(application, PvxDbHelper.DB_NAME)) {
                Log.d("TAG:MainViewModel", "Initialization")
                val retrofit = Retrofit.Builder()
                    .baseUrl("http://tmp.peoplevox.net")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

                val pvxService = retrofit.create(PvxService::class.java)

                disposables.add(pvxService.obtainData()
                    .subscribeOn(Schedulers.io())
                    .subscribe { response ->
                        val db = PvxDbHelper(response, application.applicationContext).writableDatabase
                        db.close()
                        emitter.onComplete()
                    })
            } else {
                emitter.onComplete()
            }
        }

        disposables.add(completableDbCreation.subscribe {
            database = PvxDbHelper(context = application.applicationContext).readableDatabase
            database?.apply {
                locationsRepository = LocationsRepository(this)
                itemsRepository = ItemsRepository(this)
            }
        })
    }


    private fun doesDatabaseExist(context: Context, dbName: String): Boolean {
        val dbFile = context.getDatabasePath(dbName)
        return dbFile.exists()
    }

    override fun onCleared() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
        database?.apply {
            if (isOpen) {
                close()
            }
        }
        super.onCleared()
    }

}